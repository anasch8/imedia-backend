package com.imedia.stockapi.controller;

import com.imedia.stockapi.dto.ProductDto
import com.imedia.stockapi.product.ProductResponse
import com.imedia.stockapi.service.ProductService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@RestController
@RequestMapping("/products")
class ProductController(private val productService: ProductService) {


    @GetMapping("/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        return ResponseEntity.ok(productService.findProductBySku(sku));
    }


    @GetMapping("/select", produces = ["application/json;charset=utf-8"])
    fun findProductsBySkus(
        @RequestParam("skus") skus: List<String>
    ): ResponseEntity<List<ProductResponse>> {
        return ResponseEntity.ok(productService.findProductsBySkus(skus));
    }

    @PostMapping
    fun createProduct(@Valid @RequestBody dto: ProductDto): ResponseEntity<ProductResponse> {
        return ResponseEntity.ok(productService.createProduct(dto));
    }

    @PutMapping("/{sku}")
    fun updateProductBySku(
        @PathVariable("sku") sku: String,
         @Valid @RequestBody updateDto: ProductDto
    ): ResponseEntity<ProductResponse> {
        return ResponseEntity.ok(productService.updateProduct(sku, updateDto));
    }

}

