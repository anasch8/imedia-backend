package com.imedia.stockapi.controller

import com.imedia.stockapi.exceptions.ProductNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice


@RestControllerAdvice
class ErrorHandler {

    @ExceptionHandler(ProductNotFoundException::class)
    fun handleProductNotFoundException(exception: ProductNotFoundException): ResponseEntity<RestErrorBody> {
        val errorBody = buildRestErrorBody(exception.message)
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorBody)
    }

    private fun buildRestErrorBody(exceptionMessage: String?): RestErrorBody {
        return RestErrorBody(
            RestErrorDetails("IMEDIA-SHOP-00000", exceptionMessage ?: "")
        )
    }

    data class RestErrorDetails(
        val errorCode: String,
        val message: String
    )

    data class RestErrorBody(
        val errors: RestErrorDetails
    )
}


