package com.imedia.stockapi.service

import com.imedia.stockapi.db.entity.ProductEntity
import com.imedia.stockapi.db.repository.ProductRepository
import com.imedia.stockapi.dto.ProductDto
import com.imedia.stockapi.exceptions.ProductNotFoundException
import com.imedia.stockapi.product.ProductResponse
import com.imedia.stockapi.product.ProductResponse.Companion.toProductResponse
import org.springframework.stereotype.Service

@Service
class ProductService(private val productRepository: ProductRepository) {

    //@PostConstruct
    //fun populateProducts() {
    //    val products = listOf(
    //        ProductEntity(
    //            name = "product1", description = "description1", price = BigDecimal(10),
    //            createdAt = ZonedDateTime.now(), updatedAt = ZonedDateTime.now(), quantity = 90
    //        ),
    //        ProductEntity(
    //            name = "product2", description = "description2", price = BigDecimal(20),
    //            createdAt = ZonedDateTime.now(), updatedAt = ZonedDateTime.now(), quantity = 70
    //        ),
    //        ProductEntity(
    //            name = "product3", description = "description3", price = BigDecimal(30),
    //            createdAt = ZonedDateTime.now(), updatedAt = ZonedDateTime.now(), quantity = 30
    //        )
    //    );
    //    if (productRepository.findAll().toList().isEmpty()) {
    //        productRepository.saveAll(products);
    //    }
    //}

    fun findProductBySku(sku: String): ProductResponse? {
        return productRepository.findBySku(sku)?.toProductResponse()
            ?: throw ProductNotFoundException("Could not find Product with sku: $sku"); }

    fun findProductsBySkus(skus: List<String>): List<ProductResponse>? {
        val products = productRepository.findBySkuIn(skus);
        if (products.isEmpty()) {
            throw ProductNotFoundException("Could not find Product with an sku from the provided list: $skus")
        }
        return products.map { it.toProductResponse() };
    }

    fun updateProduct(sku: String, updateDto: ProductDto): ProductResponse? {
        val productToUpdate = productRepository.findBySku(sku)
            ?: throw ProductNotFoundException("Could not find product with sku: $sku")
        productToUpdate.apply {
            description = updateDto.description
            price = updateDto.price
            name = updateDto.name
        }
        return productRepository.save(productToUpdate).toProductResponse()

    }

    fun createProduct(dto: ProductDto): ProductResponse? {
        return productRepository.save(ProductEntity.fromDto(dto)).toProductResponse();
    }

}
