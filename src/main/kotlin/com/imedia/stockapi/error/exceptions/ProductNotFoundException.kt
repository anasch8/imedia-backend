package com.imedia.stockapi.exceptions

class ProductNotFoundException(message: String) : RuntimeException(message) {}
