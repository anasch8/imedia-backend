package com.imedia.stockapi.db.entity

import com.imedia.stockapi.dto.ProductDto
import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "products")
data class ProductEntity(

    @Id
    @GeneratedValue(generator = "uuid-2", strategy = GenerationType.AUTO)
    @GenericGenerator(name = "uuid-2", strategy = "uuid2")
    @Column(name = "sku", nullable = false)
    var sku: String = "",

    @Column(name = "name", nullable = false)
    var name: String,

    @Column(name = "description")
    var description: String? = null,

    @Column(name = "price", nullable = false)
    var price: BigDecimal,

    @UpdateTimestamp
    @Column(name = "created_at", nullable = false)
    val createdAt: ZonedDateTime = ZonedDateTime.now(),

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    val updatedAt: ZonedDateTime = ZonedDateTime.now(),

    @Column(name = "quantity", nullable = false)
    val quantity: Int
) {
    companion object {
        fun fromDto(dto: ProductDto): ProductEntity {
            return ProductEntity(
                price = dto.price,
                description = dto.description,
                name = dto.name,
                quantity = dto.quantity,
            )
        }
    }
}
