package com.imedia.stockapi.db.repository

import com.imedia.stockapi.db.entity.ProductEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductRepository : JpaRepository<ProductEntity, String> {

    fun findBySku(sku: String): ProductEntity?

    fun findBySkuIn(skus: List<String>): List<ProductEntity>
}
