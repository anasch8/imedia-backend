package com.imedia.stockapi.dto

import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor
import java.math.BigDecimal

@Data
@AllArgsConstructor
@NoArgsConstructor
data class ProductDto(

    val name: String,
    val price: BigDecimal,
    val description: String,
    val quantity: Int

)
