package com.imedia.stockapi.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.imedia.stockapi.db.entity.ProductEntity
import com.imedia.stockapi.dto.ProductDto
import com.imedia.stockapi.product.ProductResponse.Companion.toProductResponse
import com.imedia.stockapi.service.ProductService
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.ArgumentMatchers.anyList
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import java.math.BigDecimal
import java.time.ZonedDateTime


@WebMvcTest
@AutoConfigureMockMvc
internal class ProductControllerTest {

    @Autowired
    lateinit var mockMvc: MockMvc

    @MockBean
    lateinit var productService: ProductService


    val baseUrl = "/products"

    @BeforeEach
    fun setUp() {
    }

    @AfterEach
    fun tearDown() {
    }

    @Test
    fun testFindProductsBySkus() {
        val objectMapper = ObjectMapper()
        val productsList = listOf(
            ProductEntity(
                name = "product1", description = "description1", price = BigDecimal(10),
                createdAt = ZonedDateTime.now(), updatedAt = ZonedDateTime.now(), quantity = 90
            ),
            ProductEntity(
                name = "product2", description = "description2", price = BigDecimal(20),
                createdAt = ZonedDateTime.now(), updatedAt = ZonedDateTime.now(), quantity = 70
            )
        ).map { it.toProductResponse() }

        Mockito.`when`(productService.findProductsBySkus(anyList())).thenReturn(productsList)

        mockMvc.get(baseUrl + "/select?skus=1,2,3").andExpect { status { isOk() } }
            .andExpect { content { contentType(MediaType.APPLICATION_JSON_UTF8_VALUE) } }
            .andExpect { content { objectMapper.writeValueAsString(productsList) } }
    }

    @Test
    fun testCreateProduct() {
        val objectMapper = ObjectMapper()

        val productDto =
            ProductDto(name = "product1", description = "description1", price = BigDecimal(10), quantity = 30);

        val expectedResponse = ProductEntity.fromDto(productDto).toProductResponse()

        Mockito.`when`(productService.createProduct(productDto)).thenReturn(expectedResponse)

        mockMvc.post(baseUrl) {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(productDto)
        }.andExpect { status { isOk() } }
            .andExpect { content { contentType(MediaType.APPLICATION_JSON) } }
            .andExpect { content { objectMapper.writeValueAsString(expectedResponse) } }
    }
}

/*
@Test
fun shouldCreateProduct() {
}
}
*/
